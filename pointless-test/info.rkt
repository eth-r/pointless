#lang info

(define collection 'multi)

(define deps
  '())
(define build-deps
  '("base"
    "rackunit-lib"
    "rackunit-chk"
    "pointless-lib"
    "predicates"))
