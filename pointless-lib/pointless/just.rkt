#lang racket/base

(provide just
         nil
         yep
         nope
         itself)

(define ((just val) . args) val)

(define nil (just '()))

(define yep (just #t))

(define nope (just #f))

(define (itself x) x)
