#lang scribble/manual

@(require (for-label racket
                     pointless
                     (except-in predicates
                                if?
                                when?
                                unless?))
          scribble/example)

@(define pointless-eval
   (make-eval-factory '(racket pointless predicates)))

@(define-syntax-rule (pointless example ...)
   (examples #:eval (pointless-eval) example ...))

@title{pointless}

@author{promethea Ow0}

@defmodule[pointless]

A library for easy tacit programming with minimal syntax overhead. And when
I say ``minimal'' I mean @italic{minimal}; it is possible to write almost
entire libraries without naming more than a couple of arguments explicitly.

Heavily based on @hyperlink["https://docs.racket-lang.org/point-free/index.html"]{
point-free} by Jack Firth, and further customized with additional control flow
operations while minimizing dependencies.

@table-of-contents[]

@section{Introduction}

@codeblock|{
#lang curly-fn racket

(require pointless predicates)

(def/cond? join-strings/symbols
  list? (λ~> #{filter (or? string? symbol?)}
             #{map (if? symbol?
                        symbol->string)}
             #{string-join % " "})
  symbol? symbol->string
  string? identity)
}|

The above is essentially the same as:

@codeblock|{
#lang racket

(define (join-strings/symbols it)
  (cond [(list? it)
         (string-join
           (map (λ (x)
                  (if (symbol? x)
                      (symbol->string x)
                      x))
                (filter (λ (y)
                          (or (string? y)
                              (symbol? y)))
                        it))
           " ")]
        [(symbol? it) (symbol->string it)]
        [(string? it) it]))
}|

This is @racket[pointless]. Everything else is commentary.

As you can hopefully observe, sometimes point-free style is easier for the reader, as it avoids
redundant naming of arguments and makes the flow of values more explicit.

Apart from the definition forms, everything in pointless is made of functions which means you can
even pass them as arguments to other functions. The exact implications of this are left as an
exercise to the reader.

@section{Threading}

@subsection{Basic Threading}

@defproc[(λ~> [f procedure?] ...)
         procedure?]{

Creates a function which sequentially applies each @racket[f] into its
input(s). Essentially the inverse of @racket[compose].

@(pointless
  ((λ~> first symbol->string string-length) '(this that))
  ((λ~> string-append string-length) "multiple" "input" "values"))

}

@defproc[(λ*> [f procedure?] ...)
         procedure?]{

Creates a function which is otherwise like @racket[λ~>] but turns multiple
input values into a single list first.

@(pointless
  ((λ*> second sqrt) 4 9 16))

}

@defproc[(~> [a any/c] [f procedure?] ...)
         any]{

Applies the @racket[f]s to @racket[a] in order. Only accepts a single input
value.

@(pointless
  (~> 15 add1 sqrt))

}

@defproc[((~>* [val any/c] ...) [f procedure?] ...)
         any]{

Creates a function which takes in a number of functions and applies them to
@racket[val]s.

@(pointless
  ((~>* 9 16) + sqrt))

}

@subsection{Short-Circuiting Threading}

@defproc[(λand~> [f procedure?] ...)
         procedure?]{

A short-circuiting version of @racket[λ~>], the resulting function returns
@racket[#false] if any of the @racket[f]s returns false.

Unlike the version in @racket[point-free], this one can take in multiple
values as long as all procedures return only a single value.

@(pointless
  (define length-after-one (λand~> (λ (lst) (member 1 lst)) cdr length))
  (length-after-one (list 0 1 2 3))
  (length-after-one (list 0 2 4 6))
  (define length-after-two (λand~> (λ lst (member 2 lst)) cdr length))
  (length-after-two 0 1 2 3)
  (length-after-two 0 1 3 9))
}

@deftogether[(@defproc[(and~> [val any/c]
                              [f procedure?] ...)
                       any]
              @defproc[((and~>* [val any/c] ...)
                        [f procedure?] ...)
                       any])]{

Short-circuiting versions of @racket[~>] and @racket[~>*], respectively.

@(pointless
  (define (find-cats str)
    (regexp-match #rx"cat.*" str))
  ((and~>* "a c" "atg" "irl") string-append find-cats first string-upcase)
  ((and~>* "a " "dog") string-append find-cats first string-upcase))

}

@section{Point-free Conditionals}

Pointless provides functions for easily defining the flow of data within your
programs in a point-free way. When combined with @racket[curly-fn]
you can easily avoid explicitly naming a single argument in many functions.

@margin-note{The normal forms @racket[if? when? unless? cond?] and @racket[if~> when~> cond~>]
             take in only a single argument.

             Forms suffixed with a @racket[*?] or @racket[*>] accept multiple arguments.}

@subsection{Basic Conditionals}

@defproc[((if? [pred (-> any/c boolean?)] [f (-> any/c any)] [g (-> any/c any) identity]) [a any/c])
          any]{

Returns a procedure which applies either @racket[f] or @racket[g] to @racket[a]
depending on the results of @racket[(pred a)].

@(pointless
  (define my-abs (if? negative? abs))
  (my-abs 3)
  (my-abs -8)
  (define vector/list-length (if? vector? vector-length length))
  (vector/list-length #(1 2 3 4))
  (vector/list-length '(1 2 3)))

}

@deftogether[(@defproc[(when? [pred (-> any/c boolean)]
                              [f (-> any/c any)])
                       (-> any/c (or/c any void?))]

              @defproc[(unless? [pred (-> any/c boolean)]
                                [f (-> any/c any)])
                       (-> any/c (or/c any void?))])]{

Returns a function that, for an input @racket[v], returns @racket[(when (pred v) (f v))]
or @racket[(unless (pred v) (f v))], respectively.

}

@defproc[(cond? [pred (-> any/c boolean)] [f (-> any/c any)]
                ... ...
                [#:else else (-> any/c any) void])
         (-> any/c any)]{

Allows the definition of multiple cases simultaneously. Yes, it's a procedure,
not syntax. Yes, its implementation is sort of ugly under the hood. Yes, it
works exactly as intended.

When @racket[cond?] has an odd number of arguments, the last argument is used
as the else clause; otherwise the optional keyword @racket[#:else] denotes the
clause to be used. The default @racket[void] currently used may get replaced with
a stricter version later, so don't rely on that behavior to not throw errors or
interesting monads (parametrizing the default else-clause is one option).

@(pointless
  (define ->string
    (cond? string? identity
           symbol? symbol->string
           number? number->string
           list? list->string
           #:else void))
  (->string "a string")
  (->string 'a-symbol)
  (->string 123)
  (->string '(#\c #\h #\a #\r #\s))
  (->string #false))

}

@subsection{Threading Conditionals}
@subsection{Multi-Argument Conditionals}
@subsection{Threading Multi-Argument Conditionals}

Still here with me? That's reasonably impressive. These are absolutely horrible
and I hope you love them exactly as much as I do. If you ever actually use any
of them in production, let me know :3c

@section{Fork}

The @racket[-<] combinator lets one send a single input to multiple functions,
collecting them together with one function.

@subsection{The Fork Combinator}

@defproc[((-< [f procedure?]
              [g (-> any/c any)] ...)
          [a any] ...)
         any]{

First applies each @racket[g] to @racket[a]s and then calls @racket[f] with the
outputs as its arguments. Honestly, it's easier to just show than tell:


@(pointless
  (define square-plus (-< + sqr add1))
  (square-plus 4)
  (square-plus 7))

}

@defproc[((-<* [f procedure?]
               [g (-> (listof any/c) any)] ...)
           [a any] ...)
           any]{

Like @racket[-<], but turns multiple input arguments into a list first.

@(pointless
  ((-<* + second third) 1 2 3)
  ((-<* cons car reverse) 1 2 3))

}

@section{Just}

When dealing with point-free functions, sometimes you want to just return values
instead of passing arguments to further functions. This is just what the
@racket[just] function is for.

@subsection{The just Function}

@defproc[(just [x any]) (-> any x)]{

Applying @racket[just] to value @racket[x] returns a function which returns
@racket[x] regardless of input.

}

@subsection{Commonly Used just-Values}

@deftogether[(@defproc[(yep [a any] ...) #t]
              @defproc[(nope [a any] ...) #f]
              @defproc[(nil [a any] ...) '()])]{

Ready-made shorthands for @racket[(just true)], @racket[(just false)], and @racket[(just null)],
respectively. Just like @racket[void].

}


@section{Definition Forms}

@subsection{Threading}

@defform[(def~> id body ...+)
          #:contracts ([body procedure?])]{
The same as @racket[(define (id . args) (apply (λ~> body ...) args))].

A very important macro for point-free programming, because otherwise definitions
referring to each other will not work.

}

@defform[(def*> id body ...+)
          #:contracts ([body procedure?])]{
The same as @racket[(define (id . args) ((λ~> body ...) args))].

}

@defform[(def/and~> id body ...+)
         #:contracts ([body procedure?])]{
The same as @racket[(define (id args ...) ((λand~> body ...) args ...))]

}

@subsection{Fork}

@defform[(def-< id body ...+)
         #:contracts ([body procedure?])]{
The same as @racket[(define (id args ...) ((-< body ...) args ...))]

}

@subsection{Conditionals}

@deftogether[(@defform[(def/if? id p f g)]
              @defform[(def/when? id p f)]
              @defform[(def/unless? id p f)]
              @defform[(def/cond? id body ...)]
             )]{

The same as @racket[(def~> id (if? p f g))], @racket[(def~> id (when? p f))],
@racket[(def~> id (unless? p f))] and @racket[(def~> id (cond? body ...))],
respectively.

}

@deftogether[(@defform[(def/if~> id p f g)]
              @defform[(def/when~> id p f)]
              @defform[(def/cond~> id body ...)]
             )]

@deftogether[(@defform[(def/if*? id p f g)]
              @defform[(def/when*? id p f)]
              @defform[(def/unless*? id p f)]
              @defform[(def/cond*? id body ...)]
             )]

@deftogether[(@defform[(def/if*> id p f g)]
              @defform[(def/when*> id p f)]
              @defform[(def/cond*> id body ...)]
             )]{

You can probably guess what these will do.

}

@subsection{Wind}

@deftogether[(@defform[(def/wind id f (pre ...) (post ...))]
              @defform[(def/wind-pre id f pre ...)]
              @defform[(def/wind-post id f post ...)]
              @defform[(def/wind* id f pre post)]
              @defform[(def/wind-pre* id f pre)]
              @defform[(def/wind-post* id f post)]
)]{

Point-free definitions for @racket[wind] etc.

}

@section{Imported Features}

See the documentation for @racket[point-free].

@subsection{Fixpoint}

Fixpoint.

@subsection{Parallel Composition}

Honestly, I don't quite grok these ones yet. Just check the original docs
for now, mmkay?

@subsection{Argument Count Syntax}

Argument count syntax.
