# pointless

[![build status](https://gitlab.com/promethea_o/pointless/badges/master/build.svg)](https://gitlab.com/promethea_o/pointless/commits/master) [![codecov](https://codecov.io/gl/promethea_o/pointless/branch/master/graph/badge.svg)](https://codecov.io/gl/promethea_o/pointless)

A #lang and functions for tacit programming in Racket. The package `pointless-lib` comes
with minimal dependencies, while providing many useful functions and macros for
easier writing of complex functions in point-free style:

```racket
#lang curly-fn racket

(require pointless
         (except-in predicates
                    if? when? unless?))

(def/cond? join-strings/symbols
  list? (λ~> #{filter (or? string? symbol?)}
             #{map (if~> symbol?
                         symbol->string)}
             #{string-join % " "})
  symbol? symbol->string
  string? identity)
```

The above does essentially the same as:

```racket
#lang racket

(define (join-strings/symbols it)
  (cond [(list? it)
         (string-join
           (map (λ (x)
                  (if (symbol? x)
                      (symbol->string x)
                      x))
                (filter (λ (y)
                          (or (string? y)
                              (symbol? y)))
                        it))
           " ")]
        [(symbol? it) (symbol->string it)]
        [(string? it) it]))
```

As you can hopefully observe, sometimes point-free style is easier for the reader, as it avoids
redundant naming of arguments and makes the flow of values more explicit.

Apart from the definition forms, everything in pointless is made of functions which means you can
even pass them as arguments to other functions. The exact implications of this are left as an
exercise to the reader.

Pointless is forked from [point-free](https://github.com/jackfirth/point-free) and heavily
customized and extended with control flow procedures and more complex definition forms.

Further documentation will be available in `pointless-doc` but let this be a concise (if obscure)
illustration of the features within:

## Threading:

```racket
> (~> (list 'this 'that)
      first
      symbol->string
      string-length)
4
> (def~> symbol-length
    symbol->string
    string-length)
> (symbol-length 'this)
4
```

## Forking:

The `-<` combinator takes in `(-< f . fs)` and returns a function which applies `f` to the results
of applying each function to the input:

```racket
> (define second+third
    (-< values second third))
> (second+third (list 1 2 3 4))
2
3
```

## Conditionals:

Function `if?` takes in `(if? cond-proc? then-proc else-proc)` and returns a function which
applies `then-proc` to the input if applying `cond-proc?` to the input returns true, and `else-proc`
otherwise.

Because it is highly convenient to do so, it assumes `identity` as default for the `else-proc` such
as in the first example. When you do not want defaulting to `identity`, use `when?` which returns
`void` instead.

For more complex operations, `cond?` is provided, with the optional keyword argument `#:else`.

## Consts:

Sometimes you want to return a specific value instead of applying a procedure to the input. In these
cases `const` from `racket/function` is your friend, but to keep our deps light `pointless` provides
`(just x)` which returns a lambda which discards its input and returns ...just `x`. For the most
common cases, `yep`, `nope`, and `nil` are provided for `(just true)`, `(just false)` and
`(just null)` respectively:

```racket
(def/cond? attribute?
  non-empty-string? ascii-only?
  symbol? (λ~> symbol->string
               ascii-only?)
  attribute-lambda? yep
  nonempty-list? (all? attribute?)
  #:else nope)
```

## Definitions:

When dealing with point-free functions in regular Racket, you might've come across the problem of
defining recursive functions with the normal means:

```racket
> (define odd?
    (λ (x) (if (= 0 x) #f (even? (- x 1)))))
  (define even?
    (λ (x) (if (= 0 x) #t (odd? (- x 1)))))
ERROR: cannot reference identifier before definition or somesuch
```

Pointless provides a solution with `def~>` which wraps its arguments into a point-free function:

```racket
> (def~> odd?
    (if? (=? 0)
         nope
         #{even? (- % 1)}))
  (def/if? even?
    (=? 0)
    yep
    #{odd? (- % 1)})
> (even? 4)
#t
```

For convenience, forms such as `def/if?`, `def/cond~>`, and `def/when*?` are provided.

## Goals:

To enable a programmer to write a useful application without ever naming a single argument (apart
from `%`).

Joking aside, this does make some problems significantly simpler to express.

Typed/racket really doesn't want to be friends with higher-order functions like these, but using
macros instead might be a solution.

## License:

Unless otherwise specified, all my work is published under a license which guarantees users all
negative freedoms and imposes zero positive requirements. Currently codenamed the "Don't Fricking
Sue Anybody License", all works under it are free for anyone to use and modify, without any
restrictions as long as the user similarly refrains from restricting others. As long as you don't
sue anybody over their use of your derived works, I will not cause you any trouble either.

However, to protect the same freedom of everyone else, your derived works must be licensed under the
same license. Sorry, I don't make the rules; governments make the rules and impose IP monopolies on
everyone else.

Basically, it's a strong copyleft license which is technically non-free in the most technical way
possible (only 3.5 of the 4 essential freedoms guaranteed, as I have no right to *force* you to
share source although it sure would be *nice* if you did it). Or in other words, if you assume
it's like GPL but way more chill and not interested in causing you trouble, you'd be approximately
right. I'm not going to sue you over it, as long as you don't sue anybody else over it either. But
if you're looking for software you can sue others over, move on.
